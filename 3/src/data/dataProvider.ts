import { Category } from './category';
import { Product } from './product';

import savedProducts from '../../assets/products.json';
import { ActionStatus, ErrorCategory } from './actionStatus';

export class DataProvider {
  private categories: Category[] = [];
  private products: Product[] = [];

  constructor() {
    savedProducts.forEach((product) => {
      let category = this.categories.find((category) => category.name === product.categoryName);
      if (!category) {
        category = new Category(product.categoryName);
        this.categories.push(category);
      }
      this.products.push(new Product(category.id, product.name, product.itemsInStock));
    });
  }

  //#region Products
  getProducts(): Product[] {
    return this.products;
  }

  getProduct(id: string): Product | undefined {
    return this.products.find((product) => product.id === id);
  }

  getProductsByCategory(categoryId: string): Product[] {
    return this.products.filter((product) => product.categoryId === categoryId);
  }

  addProduct(category: string, name: string, itemsInStock: number): ActionStatus {
    if (name.length < 3)
      return new ActionStatus(false, ErrorCategory.Validation, `Name ${name} doesn't meet the 3 characters minimal length criteria`);

    const product = new Product(category, name, itemsInStock);
    this.products.push(product);
    return new ActionStatus(true);
  }

  editProduct(id: string, categoryId: string, name: string, itemsInStock: number): ActionStatus {
    if (name.length < 3)
      return new ActionStatus(false, ErrorCategory.Validation, `Name ${name} doesn't meet the 3 characters minimal length criteria`);

    const product = this.getProduct(id);
    if (product) {
      product.categoryId = categoryId;
      product.name = name;
      product.itemsInStock = itemsInStock;
      return new ActionStatus(true);
    }
    return new ActionStatus(false, ErrorCategory.NotFound, `Product id ${id} was not found`);
  }

  deleteProduct(id: string): ActionStatus {
    return this.deleteItemById(this.products, id)
      ? new ActionStatus(true)
      : new ActionStatus(false, ErrorCategory.NotFound, `Product id ${id} was not found`);
  }
  //#endregion

  //#region Categories
  getCategories(): Category[] {
    return this.categories;
  }

  getCategory(id: string): Category | undefined {
    return this.categories.find((category) => category.id === id);
  }

  addCategory(name: string): ActionStatus {
    this.categories.push(new Category(name));
    return new ActionStatus(true);
  }

  editCategory(id: string, name: string): ActionStatus {
    const category = this.getCategory(id);

    if (category) {
      category.name = name;
      return new ActionStatus(true);
    }
    return new ActionStatus(false, ErrorCategory.NotFound, `Category id ${id} was not found`);
  }

  deleteCategory(id: string): ActionStatus {
    return this.deleteItemById(this.categories, id)
      ? new ActionStatus(true)
      : new ActionStatus(false, ErrorCategory.NotFound, `Category id ${id} was not found`);
  }

  deleteItemById(items: { id: string }[], id: string): boolean {
    const index = items.findIndex((item) => item.id === id);
    if (index >= 0) {
      return items.splice(index, 1).length > 0;
    }
    return false;
  }
  //#endregion
}
