import express from 'express';
import cors from 'cors';
import { ErrorCategory } from '../data/actionStatus';
import { DataProvider } from '../data/dataProvider';

export class ProductsServer {
  private readonly app = express();
  private readonly dataProvider = new DataProvider();

  Init(): void {
    this.app.use(express.json());
    this.app.use(cors());

    this.InitRoutes();

    this.app.listen(process.env.PORT || 3000, () => {
      console.log('Listening');
    });
  }

  InitRoutes(): void {
    this.app.all('/[A-Z]+/:id/:query*?', (req, res, next) => {
      const id = req.params.id;
      if (id.length !== 36) {
        res.status(400).send(`Id ${id} is not valid - uuid length should be 36`);
      } else next();
    });

    this.InitProductsRoutes();
    this.InitCategoryRoutes();
  }

  InitProductsRoutes(): void {
    this.app.get('/products', (req, res) => {
      res.status(200).send(this.dataProvider.getProducts());
    });

    this.app.get('/products/:id', (req, res) => {
      const product = this.dataProvider.getProduct(req.params.id);
      if (product) {
        res.status(200).send(product);
      } else {
        res.status(404).send(`Product id ${req.params.id} not found`);
      }
    });

    this.app.get('/categories/:id/products', (req, res) => {
      const category = this.dataProvider.getCategory(req.params.id);
      if (category) {
        res.status(200).send(this.dataProvider.getProductsByCategory(req.params.id));
      } else {
        res.status(404).send(`Category id ${req.params.id} was not found`);
      }
    });

    this.app.post('/products', (req, res) => {
      const status = this.dataProvider.addProduct(req.body.categoryId, req.body.name, req.body.itemsInStock);
      if (status.success) {
        res.status(201).send('Product added successfully');
      } else {
        res.status(409).send(status.error);
      }
    });

    this.app.put('/products/:id', (req, res) => {
      const status = this.dataProvider.editProduct(req.params.id, req.body.categoryId, req.body.name, req.body.itemsInStock);
      if (status.success) {
        res.status(200).send('Product edited successfully');
      } else {
        switch (status.errorCategory) {
          case ErrorCategory.NotFound:
            res.status(404).send(status.error);
          case ErrorCategory.Validation:
            res.status(409).send(status.error);
        }
      }
    });

    this.app.delete('/products/:id', (req, res) => {
      const status = this.dataProvider.deleteProduct(req.params.id);
      if (status.success) {
        res.status(204).send('Product deleted successfully');
      } else {
        res.status(404).send(status.error);
      }
    });
  }

  InitCategoryRoutes(): void {
    this.app.get('/categories', (req, res) => {
      res.status(200).send(this.dataProvider.getCategories());
    });

    this.app.get('/categories/:id', (req, res) => {
      const category = this.dataProvider.getCategory(req.params.id);
      if (category) {
        res.status(200).send(category);
      } else {
        res.status(404).send(`Category id ${req.params.id} was not found`);
      }
    });

    this.app.post('/categories', (req, res) => {
      this.dataProvider.addCategory(req.body.name);
      res.status(201).send('Category added successfully');
    });

    this.app.put('/categories/:id', (req, res) => {
      const status = this.dataProvider.editCategory(req.params.id, req.body.name);
      if (status.success) {
        res.status(200).send('Category edited successfully');
      } else {
        res.status(404).send(status.error);
      }
    });

    this.app.delete('/categories/:id', (req, res) => {
      const status = this.dataProvider.deleteCategory(req.params.id);
      if (status.success) {
        res.status(204).send('Category deleted successfully');
      } else {
        res.status(404).send(status.error);
      }
    });
  }
}
