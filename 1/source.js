const { EventEmitter } = require("events");

const event = new EventEmitter();
const getRandomNumber = (max) => Math.floor(Math.random() * max);

module.exports = {
  event: event,
  init: () => {
    setInterval(() => {
      event.emit("numbers", getRandomNumber(100), getRandomNumber(100));
    }, 1000);
  },
};
