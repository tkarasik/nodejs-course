module.exports = (n1, n2) =>
  new Promise((resolve, reject) => {
    if (isNaN(n1) || isNaN(n2)) {
      reject("Numbers are invalid");
    } else {
      resolve(n1 + n2);
    }
  });
