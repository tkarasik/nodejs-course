const source = require("./source");
const sum = require("./app");

source.event.on("numbers", async (n1, n2) => {
  try {
    let result = await sum(n1, n2);
    console.log(`Generated numbers: ${n1}, ${n2}\tResult: ${result}`);
  } catch (error) {
    console.log(`Promise rejected: ${error}`);
  }
});
source.init();
