const cluster = require("cluster");
const http = require("http");
const numCPUs = require("os").cpus().length;

if (cluster.isMaster) {
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }
} else {
  http.createServer((req, res) => {
      const startTime = new Date();
      const reqId = req.url.substr(1);

      console.log(`Worker ${process.pid} is handling request ${reqId}`);

      res.writeHead(200);
      Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, 2000);
      res.end();

      console.log(`Worker ${process.pid} finished handling request ${reqId} after ${Date.now() - startTime}ms`);
    }).listen(8000);

  console.log(`Worker ${process.pid} started`);
}
