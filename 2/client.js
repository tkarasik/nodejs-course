const http = require("http");

const REQUESTS = 10;
const promises = new Array(REQUESTS);

const startTime = new Date();

for (let i = 1; i <= REQUESTS; i++) {
  console.log(`Sending request ${i} after ${Date.now() - startTime}ms`);

  promises.push(
    new Promise((resolve, reject) => {
      const req = http.get(`http://localhost:8000/${i}`, (res) => {
        res.on("data", (data) => {});
        res.on("end", () => {
          console.log(`End request ${i} after ${Date.now() - startTime}ms`);
          resolve(i);
        });
      });
      req.on("error", (error) => {
        console.error(error);
        reject();
      });
      req.end();
    })
  );
}
Promise.all(promises).then((values) => {
  console.log(`Finished all ${REQUESTS} requests after ${Date.now() - startTime}ms`);
});
